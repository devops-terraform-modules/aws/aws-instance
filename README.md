<!-- BEGIN_TF_DOCS -->



## Resources

The following resources are used by this module:

- [aws_instance.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) (resource)
```yaml
locals {
  instances = [for instance in var.resources : {
    name                        = instance.name
    instance_type               = instance.instance_type
    ami                         = instance.ami
    subnet_id                   = try(instance.subnet_id != null ? instance.subnet_id : null, null)
    associate_public_ip_address = try(instance.associate_public_ip_address != null ? instance.associate_public_ip_address : null, null)
    user_data_base64            = try(instance.user_data_base64 != null ? instance.user_data_base64 : null, null)
    user_data_replace_on_change = try(instance.user_data_replace_on_change != null ? instance.user_data_replace_on_change : null, null)
    tags                        = try(instance.tags != null ? instance.tags : null, null)
  }]

  common_tags = {
    Project     = var.project.project
    Environment = var.project.environment
    CreatedBy   = var.project.createdBy
    Group       = var.project.group
  }
}

resource "aws_instance" "this" {
  for_each = { for instance in local.instances : instance.name => instance }

  ami                         = each.value.ami
  instance_type               = each.value.instance_type
  subnet_id                   = each.value.subnet_id
  user_data_replace_on_change = each.value.user_data_replace_on_change
  user_data_base64            = each.value.user_data_base64
  associate_public_ip_address = each.value.associate_public_ip_address

  tags = merge(local.common_tags, each.value.tags != null ? each.value.tags : {
    Name        = each.value.name
    Description = "Created by ${var.project.createdBy}"
  })
}
```

## Providers

The following providers are used by this module:

- <a name="provider_aws"></a> [aws](#provider\_aws) (~>5.35.0)
```yaml
terraform {
  required_version = ">1.7.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>5.35.0"
    }

    random = {
      source  = "hashicorp/random"
      version = "~>3.1.0"
    }
  }
}
```

## Required Inputs

The following input variables are required:

### <a name="input_project"></a> [project](#input\_project)

Description: some common settings like project name, environment, and tags for all resources-objects

Type:

```hcl
object({
    project     = string
    environment = string
    createdBy   = string
    group       = string
  })
```

### <a name="input_resources"></a> [resources](#input\_resources)

Description: resources of objects to create

Type:

```hcl
list(object({
    name                        = string
    instance_type               = string
    ami                         = string
    user_data_replace_on_change = optional(bool)
    user_data_base64            = optional(string)
    subnet_id                   = optional(string)
    associate_public_ip_address = optional(bool)
    tags                        = optional(map(string))
  }))
```

## Optional Inputs

No optional inputs.
```yaml
variable "resources" {
  description = "resources of objects to create"
  nullable    = false
  type = list(object({
    name                        = string
    instance_type               = string
    ami                         = string
    user_data_replace_on_change = optional(bool)
    user_data_base64            = optional(string)
    subnet_id                   = optional(string)
    associate_public_ip_address = optional(bool)
    tags                        = optional(map(string))
  }))
}

variable "project" {
  description = "some common settings like project name, environment, and tags for all resources-objects"
  nullable    = false
  type = object({
    project     = string
    environment = string
    createdBy   = string
    group       = string
  })
}


```

## Outputs

The following outputs are exported:

### <a name="output_instances_arns"></a> [instances\_arns](#output\_instances\_arns)

Description: Arns of the created EC2 instances.

### <a name="output_instances_ids"></a> [instances\_ids](#output\_instances\_ids)

Description: Ids of the created EC2 instances.

### <a name="output_public_ips"></a> [public\_ips](#output\_public\_ips)

Description: List of public IP addresses assigned to the instances, if applicable.
```yaml
output "instances_ids" {
  description = "Ids of the created EC2 instances."
  value = [
    for instance in aws_instance.this : instance.id
  ]
}

output "instances_arns" {
  description = "Arns of the created EC2 instances."
  value = [
    for instance in aws_instance.this : instance.arn
  ]
}

output "public_ips" {
  description = "List of public IP addresses assigned to the instances, if applicable."
  value = [
    for instance in aws_instance.this : instance.public_ip
  ]
}
```

## Example from YAML
```yaml
project:
  project: module_template
  region: eu-west-1
  profile: default
  createdBy: markitos
  environment: dev
  group: "webs"
instances:
  - name: example-instance-yaml-1
    ami: "ami-xxxxx"
    instance_type: "t3.micro"
    associate_public_ip_address: true
    tags:
      Name: example-instance-yaml-1
      Description: example-instance-yaml-1 of 3
  - name: example-instance-yaml-2
    ami: "ami-xxxxx"
    instance_type: "t3.micro"
    associate_public_ip_address: false
    tags:
      Name: example-instance-yaml-2
      Description: example-instance-yaml-2 of 3
  - name: example-instance-yaml-3
    ami: "ami-xxxxx"
    instance_type: "t3.micro"
    tags:
      Name: example-instance-yaml-3
      Description: example-instance-yaml-3 of 3
```

```yaml
provider "aws" {
  profile = local.manifest.project.profile
  region  = local.manifest.project.region
}

locals {
  manifest = yamldecode(file("${path.cwd}/manifest.yaml"))
}

module "module_usage_from_yaml" {
  source    = "./../.."
  project   = local.manifest.project
  resources = local.manifest.instances
}
```

## Example from code .tf
```yaml
provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}

module "module_usage_from_code" {
  source = "./../.."

  project = {
    project     = "my-project"
    environment = "dev"
    createdBy   = "markitos"
    group       = "my-group"
  }

  resources = [
    {
      name                        = "web-1"
      instance_type               = "t2.micro"
      ami                         = "ami-xxxxx"
      user_data_replace_on_change = true
      user_data_base64            = "ZWNobyAiXG5DdWx0dXJhRGV2b3AgLSBtQXJraXRvc1xuIg=="
      associate_public_ip_address = true
      tags = {
        Name = "web-1"
      }
    },
    {
      name          = "web-2"
      instance_type = "t2.micro"
      ami           = "ami-xxxxx"
    }
  ]
}
```
<!-- END_TF_DOCS -->