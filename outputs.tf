output "instances_ids" {
  description = "Ids of the created EC2 instances."
  value = [
    for instance in aws_instance.this : instance.id
  ]
}

output "instances_arns" {
  description = "Arns of the created EC2 instances."
  value = [
    for instance in aws_instance.this : instance.arn
  ]
}

output "public_ips" {
  description = "List of public IP addresses assigned to the instances, if applicable."
  value = [
    for instance in aws_instance.this : instance.public_ip
  ]
}
